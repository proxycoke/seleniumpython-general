from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

driver=webdriver.Chrome(executable_path=r"E:\chromedriver.exe")
driver.get("file:E:/SeleniumTestPython/alerts/alert_simple.html")
time.sleep(3)

alerta_simple=driver.find_element_by_name("alert")
alerta_simple.click()
time.sleep(2)

alerta_simple=driver.switch_to_alert()#en cuanto encuentra la ventana se cambia a ella
alerta_simple.dismiss()
time.sleep(3)
driver.close()