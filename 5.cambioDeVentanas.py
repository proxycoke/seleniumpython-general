import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

class case_unittest(unittest.TestCase):
	def setUp(self):
		self.driver=webdriver.Chrome(executable_path=r"E:\chromedriver.exe")

	def test_cambio_ventanas(self):
		driver=self.driver
		driver.get("https://www.google.com")
		time.sleep(3)
		driver.execute_script("window.open('');")
		time.sleep(3)
		driver.switch_to.window(driver.window_handles[1])#El handles[number] te indica que pestaña abrira en donde 0 es la pestaña de google que abre primero y 1 la de jira
		driver.get("https://jira.tid.es/secure/Dashboard.jspa")
		time.sleep(3)
		driver.switch_to.window(driver.window_handles[0])
		time.sleep(3)

if __name__ == '__main__':
	unittest.main()