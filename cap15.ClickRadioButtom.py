import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

class unittest_case(unittest.TestCase):
	def setUp(self):
		self.driver=webdriver.Chrome(executable_path=r"E:\chromedriver.exe")

	def test_uso_radio_buttom(self):
		driver=self.driver
		driver.get("https://www.w3schools.com/howto/howto_css_custom_checkbox.asp")
		time.sleep(2)
		radio_bt=driver.find_element_by_xpath("//*[@id='main']/div[3]/div[1]/input[2]")
		radio_bt.click()
		time.sleep(1)
		radio_bt=driver.find_element_by_xpath("//*[@id='main']/div[3]/div[1]/input[1]")
		radio_bt.click()
		time.sleep(2)

	def tearDown(self):
		self.driver.close()

if __name__ == '__main__':
	unittest.main()