import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

class use_unit_test(unittest.TestCase):#Especificando el Test case
	def setUp(self):
		self.driver=webdriver.Chrome(executable_path=r"E:\chromedriver.exe")

	def test_buscar(self):#IMPORTANTE SIEMPRE PONER TEST POR DELANTE 
		driver=self.driver
		driver.get("https://google.com")
		self.assertIn("Google",driver.title)
		elemento=driver.find_element_by_name("q")
		elemento.send_keys("selenium")
		elemento.send_keys(Keys.ENTER)
		time.sleep(5)
		assert"Elemento no encontrado" not in driver.page_source

	def tearDown(self):
		self.driver.close()

if __name__ == '__main__':
	unittest.main()