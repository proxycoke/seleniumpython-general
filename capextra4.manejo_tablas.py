from selenium import webdriver
import time

driver=webdriver.Chrome(executable_path=r"E:\chromedriver.exe")
driver.get("https://www.w3schools.com/html/html_tables.asp")
time.sleep(3)

# trae todo lo que trae el valor tr y nos trae la posiciónes ssolicitadas
valor=driver.find_element_by_xpath("//*[@id='customers']/tbody/tr[2]/td[2]").text 
print(valor)

#cuenta la cantidad de todas las filas 
rows=len(driver.find_elements_by_xpath("//*[@id='customers']/tbody/tr"))
#cuenta la cantidad de todas las columnas
col=len(driver.find_elements_by_xpath("//*[@id='customers']/tbody/tr[1]/th"))
print(rows)
print(col)

#ciclo for
for n in range(2,rows+1): #rangos para filas
	for b in range(1,col+1): #rangos para columnas
		dato=driver.find_element_by_xpath("//*[@id='customers']/tbody/tr["+str(n)+"]/td["+str(b)+"]").text #str convierte numero en caracter
		print(dato, end='									')#end= imprime hasta el final toda la tabla
	print()
driver.close()

