import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

class usando_unittest_xpath(unittest.TestCase):
	def setUp(self):
		self.driver=webdriver.Chrome(executable_path=r"E:\chromedriver.exe")

	def test_buscar_por_xpath(self):
		#BUSCAR EL LOGIN DE MOVISTAR PLAY
		driver=self.driver
		driver.get("https://movistarplay.com.pe")
		time.sleep(3)
		buscar_por_xpath=driver.find_element_by_xpath("/html/body/go-mdl-layout-cmp-main/header/div/go-mdl-authentication-cmp-login-button/div/p/a") #cambiar las "" por '' dentro del [@id="texto"] ya que marcara error
		time.sleep(3)
		buscar_por_xpath.send_keys("Iniciar sesión",Keys.ENTER)
		time.sleep(3)

		#BLOQUE INICIO DE SESIÓN
		user=driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/div/form[1]/div/div[2]/input")
		user.send_keys("mplayvfull@gmail.com",Keys.TAB)
		time.sleep(3)

		password=driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/div/form[1]/div/div[3]/input")
		password.send_keys("qawund3rm4n")
		password.send_keys(Keys.ENTER)
		time.sleep(3)
		#FIN BLOQUE INICIO DE SESIÓN

		liveTV=driver.find_element_by_xpath("/html/body/go-mdl-layout-cmp-main/header/div/go-mdl-layout-cmp-first-header/div/nav/a[3]")
		liveTV.send_keys("Centro de ayuda",Keys.ENTER)
		time.sleep(8)


	def tearDown(self):
		self.driver.close()
if __name__ == '__main__':
	unittest.main()








#xpath es una estructura de objetos.
#existen 2 tipos de xpath : relativo y absoluto.

#xpath relativo: parte de un nodo o modulo específico
#ejemplo: este seria el xpath del texto "iniciar sesión " en mplay /html/body/go-mdl-layout-cmp-main/header/div/go-mdl-authentication-cmp-login-button/div/p/a

#xpath absoluto: si es que le componente NUNCA cambiaria.
# recomendado es usar el xpath relativo ( REVISAR TODO CONCEPTO DE XPATH Y FUNCIONAMIENTO)
